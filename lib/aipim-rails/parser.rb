require 'aipim-rails/parser_helper'

module Parser

	def self.init(filename)
		parser(filename)
	end

	def self.tag_screenshot(line,file,workset)
		if line && ParserHelper.is_tag?(line) 
			workset[:screenshot] = true if ParserHelper.is_screenshot?(line)
			line = ParserHelper::read_line(file)
		end
		return line, workset
	end

	def self.tag_feature(line,file,workset)
		if line && ParserHelper.is_feature?(line)

			workset[:feature_name] = ParserHelper.get_feature(line)

			while ((line = ParserHelper::read_line(file)) && !ParserHelper.is_command?(line))
				workset[:feature_description] << line if !(line.gsub(" ", "") == "")
			end
			
		end
		return line, workset
	end

	def  self.tag_context(line,file,workset)
		if line && ParserHelper.is_context?(line)
			while ((line = ParserHelper::read_line(file)) && !ParserHelper.is_command?(line))
				workset[:context_description] << line if !(line.gsub(" ", "") == "")
			end
		end
		return line, workset
	end

	def self.tag_scenario(line,file,workset)
		if line && ParserHelper.is_scenario?(line)
			scenario_name = ParserHelper.get_scenario(line)
			scenario_steps = []
			while ((line = ParserHelper::read_line(file)) && !ParserHelper.is_command?(line))
				scenario_steps << line if !(line.gsub(" ", "") == "")
			end
			if workset[:screenshot]
				screenshot = workset[:screenshots][workset[:screenshot_counter]].to_s
				workset[:screenshot_counter] += 1
				workset[:screenshot] = false
			end
			workset[:scenarios] << {name: scenario_name, steps: scenario_steps, screenshot: screenshot}
		end
		return line, workset
	end

	def self.process_tags(line, file, workset)
		line, workset = tag_screenshot(line, file, workset)
		line, workset = tag_feature(line, file, workset)
		line, workset = tag_context(line, file, workset)
		line, workset = tag_scenario(line, file, workset)
		line = ParserHelper::read_line(file) unless ParserHelper.is_command?(line)
		return line, workset
	end

	def self.parser(filename)
		# Read input
		file = read_file(filename)

		# Get screenshot files
		screenshots = get_screenshot_files(filename)

		workset = {
			screenshot: false,
			screenshot_counter: 0,
			screenshots: screenshots,
			feature_name: "",
			feature_description: [],
			context_name: "",
			context_description: [],
			scenarios: []
		}

		line = ParserHelper::read_line(file)
		while (line)
			line, workset = process_tags(line,file,workset)
		end
		file.close

		feature = {
			feature_name: workset[:feature_name],
			feature_description: workset[:feature_description],
			context_name: workset[:context_name],
			context_description: workset[:context_description],
			scenarios: workset[:scenarios],
			filename: filename
		}


	end

	def self.read_file(filename)
		filename = "features/"+filename
		raise "FileNotFound" unless File.exists?(filename)
		File.open(filename, "r")
	end

	def self.get_screenshot_files(filename)
		if File.directory?("aipim/screenshots/#{filename}")
			ret = %x[\ls aipim/screenshots/#{filename}].split("\n")
		else
			ret = []
		end
		ret
	end

end
