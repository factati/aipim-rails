require 'aipim-rails/parser_helper'

module Html

	def self.get_tag_color(str, word, classname)
		str.gsub(word,'<font class="'+classname+'">'+word+'</font>')
	end

	def self.get_tag_quotes(str)
		quotes = []
		tquotes = str.scan(/"([^"]*)"/)
		tquotes.each { |t| quotes << t[0] }
		quotes = quotes.uniq
		quotes.each { |t| str = str.gsub('"'+t+'"', '"<font class="tag-quotes">'+t+'</font>"') }
		str
	end

	def self.colorize(str)
		str = get_tag_quotes(str)
		str = get_tag_color(str, "Dado ", "tag-given")
		str = get_tag_color(str, "Quando ", "tag-when")
		str = get_tag_color(str, "Então ", "tag-then")
		str = get_tag_color(str, "E ", "tag-and")
	end


	def self.index(features)

		output = File.open("aipim/html/index.html", "w")
		output.puts '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<META http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="bootstrap.min.css" rel="stylesheet">
		<link href="costum/costum.css" rel="stylesheet">
		<script src="jquery-1.9.1.js"></script>
		<script src="costum/costum.js"></script>
	</head>
<body>
	<div class="panel panel-default scenario-index-panel">
		<div class="panel-body scenario-index-body">
			<ul class="list-unstyled">'
		features.each_with_index do |feature,i|
			output.puts '
				<li><a href="'+feature[:filename]+'.html">'+(i+1).to_s+'. '+feature[:feature_name]+'</a></li>'
		end
		output.puts '
			</ul>
		</div>
	</div>
</body>
</html>'

		output.close
	end

	def self.features(features)

		features.each_with_index do |feature, i| 
			output = File.open("aipim/html/"+feature[:filename]+".html", "w")
			output.puts '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<META http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="bootstrap.min.css" rel="stylesheet">
		<link href="costum/costum.css" rel="stylesheet">
		<script src="jquery-1.9.1.js"></script>
		<script src="costum/costum.js"></script>
	</head>
<body>
	<div class="panel panel-default scenario-index-panel">
		<div class="panel-body scenario-index-body">
			<ul class="list-unstyled">'
			features.each_with_index do |t, j|
				output.puts '
				<li><a href="'+t[:filename]+'.html">'+(j+1).to_s+'. '+t[:feature_name]+'</a></li>'
				if i == j
					output.puts '
					<li>
						<ul class="list-unstyled features-link">'
					feature[:scenarios].each_with_index do |scenario,k|
						output.puts '
							<li><a href="#">'+(j+1).to_s+'.'+(k+1).to_s+'. '+scenario[:name]+'</a></li>'
					end
					output.puts '
						</ul>
					</li>'
				end
			end
			output.puts '
			</ul>
		</div>
	</div>'

			feature[:scenarios].each_with_index do |scenario,j|

				output.puts '
	<div class="panel panel-default scenario-panel">
		<div class="panel-heading scenario-name">
    		<h3 class="panel-title">'+(i+1).to_s+'.'+(j+1).to_s+'. '+scenario[:name]+'<span class="pull-right"><a href="#">[índice]</a></span></h3>
  		</div>
  		<div class="panel-body scenario-body">
			<ul class="list-unstyled">'
				scenario[:steps].each do |step|
					output.puts '
				<li>'+colorize(step)+'</li>'
	        	end

	        	output.puts '
		    </ul>
		</div>'
			
				if scenario[:screenshot]

					output.puts '
  		<div class="panel-footer scenario-screenshot">
  			<a href="../screenshots/'+feature[:filename]+'/'+scenario[:screenshot]+'"><img src="../screenshots/'+feature[:filename]+'/'+scenario[:screenshot]+'" /></a>
  		</div>'
				end

				output.puts '
	</div>'

			end

			output.puts '	
</body>
</html>'

			output.close
		end
	end
	
end
