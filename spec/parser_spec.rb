#require 'spec_helper'
require './lib/aipim-rails'

describe Parser do

	let(:filename) { "myfeature.feature" }
	let(:file) { double(File) }

	describe "Init" do
		it 'Call parser method' do
			Parser.stub(:parser)
			Parser.should_receive(:parser).with("filename").once
			Parser.init("filename")
		end
	end

	describe 'Read file' do
		before do
			# Verifica se arquivo existe antes
			File.should_receive(:exists?).with("features/"+filename).once
			# Verifica se tentou abrir arquivo
			File.should_receive(:open).with("features/"+filename, "r").once
		end
		it 'file found' do
			#expect {Parser.read_file("asdsd")}.to raise_error("FileNotFound")
			File.stub(:exists?).with("features/"+filename).and_return(true)
			File.stub(:open).with("features/"+filename, "r").and_return(file)
			expect(file).to eq(Parser.read_file(filename))
		end
	end

	describe 'Tags' do

		let(:line1) 	{"linha1"}
		let(:line2) 	{"linha2"}
		let(:workset) 	{{
				feature_name: "",
				feature_description: [],
				context_description: [],
				screenshot: true,
				screenshots: ["myss.png"],
				screenshot_counter: 0,
				scenarios: []
			}}
		let(:lines) {[5]}

		before do
			lines[0] = "This the first line of ..."
			lines[1] = "... the ... ops, now I am on second line."
			lines[2] = "   "
			lines[3] = "Somehow the last line became useless!"
			lines[4] = ""
			ParserHelper.stub(:read_line).with(file).and_return(
				lines[0],
				lines[1],
				lines[2],
				lines[3],
				lines[4],
				"<command>")
			ParserHelper.stub(:is_command?).and_return(false)
			ParserHelper.stub(:is_command?).with("<command>").and_return(true)
		end

		describe 'Screenshot' do

			let(:current_line) {"@javascript @screenshot"}

			before do
				ParserHelper.stub(:is_tag?).with(current_line).and_return(true)
			end

			it 'found!' do
				workset[:screenshot] = false
				ParserHelper.stub(:is_screenshot?).with(current_line).and_return(true)
				new_line, new_workset = Parser.tag_screenshot(current_line, file, workset)
				expect(new_line).to eq(lines[0])
				expect(new_workset[:screenshot]).to eq(true)
			end

			it 'not found!' do
				ParserHelper.stub(:is_screenshot?).with(current_line).and_return(false)
				new_line, new_workset = Parser.tag_screenshot(current_line, file, workset)
				expect(new_line).to eq(lines[0])
				expect(new_workset).to eq(workset)
			end
		end

		describe 'Feature' do

			let(:current_line) {"Feature: myfeature"}

			before do
				ParserHelper.stub(:get_feature).and_return("myfeature")
			end

			it 'found!' do
				ParserHelper.stub(:is_feature?).with(current_line).and_return(true)

				new_line, new_workset = Parser.tag_feature(current_line, file, workset)

				expect(new_line).to eq("<command>")
				expect(new_workset[:feature_name]).to eq("myfeature")
				expect(new_workset[:feature_description]).to eq(
					["This the first line of ...",
					"... the ... ops, now I am on second line.",
					"Somehow the last line became useless!"])

			end

			it 'not found!' do
				ParserHelper.stub(:is_feature?).with(current_line).and_return(false)

				new_line, new_workset = Parser.tag_feature(current_line, file, workset)

				expect(new_line).to eq(current_line)
				expect(new_workset).to eq(workset)
			end
		end

		describe 'Context' do

			let(:current_line) { "Context: " }

			it 'found!' do
				ParserHelper.stub(:is_context?).with(current_line).and_return(true)

				new_line, new_workset = Parser.tag_context(current_line, file, workset)

				expect(new_line).to eq("<command>")
				expect(new_workset[:context_description]).to eq(
					["This the first line of ...",
					"... the ... ops, now I am on second line.",
					"Somehow the last line became useless!"])

			end


			it 'not found!' do
				ParserHelper.stub(:is_context?).with(current_line).and_return(false)

				new_line, new_workset = Parser.tag_context(current_line, file, workset)

				expect(new_line).to eq(current_line)
				expect(new_workset).to eq(workset)
			end
		end

		describe 'Scenario' do

			let(:current_line) { "Scenario: myscenario" }

			before do
				ParserHelper.stub(:get_scenario).with(current_line).and_return("myscenario")
			end
		
			it 'found!' do
				workset[:screenshot] = true
				ParserHelper.stub(:is_scenario?).with(current_line).and_return(true)

				new_line, new_workset = Parser.tag_scenario(current_line, file, workset)

				expect(new_workset[:screenshot]).to eq(false)
				expect(new_workset[:screenshots]).to eq(workset[:screenshots])
				expect(new_workset[:screenshot_counter]).to eq(1)
				expect(new_workset[:scenarios]).to eq([{
					name: "myscenario",
					steps: ["This the first line of ...",
					"... the ... ops, now I am on second line.",
					"Somehow the last line became useless!"],
					screenshot: "myss.png"
				}])
			end

			it 'not found!' do
				ParserHelper.stub(:is_scenario?).with(current_line).and_return(false)

				new_line, new_workset = Parser.tag_scenario(current_line, file, workset)

				expect(new_line).to eq(current_line)
				expect(new_workset).to eq(workset)
			end

		end

	end

	describe 'Process Tags' do

		let(:lines) {["line1", "line2", "line3", "line4", "line5", "line6"]}
		let(:worksets) {["ws1", "ws2", "ws3", "ws4", "ws5"]}

		before do
			Parser.stub(:tag_screenshot).with(lines[0], file, worksets[0]).and_return([lines[1], worksets[1]])
			Parser.stub(:tag_feature).with(   lines[1], file, worksets[1]).and_return([lines[2], worksets[2]])
			Parser.stub(:tag_context).with(   lines[2], file, worksets[2]).and_return([lines[3], worksets[3]])
			Parser.stub(:tag_scenario).with(  lines[3], file, worksets[3]).and_return([lines[4], worksets[4]])
			ParserHelper.stub(:is_command?).with(lines[4]).and_return(false)
			ParserHelper.stub(:read_line).with(file).and_return(lines[5])
		end

		it 'should call all tag methods' do

			line, workset = Parser.process_tags(lines[0], file, worksets[0])

			expect(line).to eq(lines[5])
			expect(workset).to eq(worksets[4])

		end

	end

	describe 'Parser' do

		let(:lines) {[2]}
		let(:workset) {{
				screenshot: false,
				screenshot_counter: 0,
				screenshots: [],
				feature_name: "",
				feature_description: [],
				context_name: "",
				context_description: [],
				scenarios: []
			}	
		}

		before do

			lines[0] = "<begin scenario>"
			lines[1] = "<end scenario>"

			Parser.stub(:read_file).with(filename).and_return(file)
			ParserHelper.stub(:read_line).with(file).and_return(lines[0])
			Parser.stub(:get_screenshot_files).with(filename).and_return([])
			Parser.stub(:process_tags).with(lines[0], file, workset).and_return([lines[1], workset])
			Parser.stub(:process_tags).with(lines[1],file, workset).and_return([nil, workset])
			file.stub(:close)

		end

		it 'should parser' do

			feature = Parser.parser(filename)

			expect(feature).to eq({
				feature_name: workset[:feature_name],
				feature_description: workset[:feature_description],
				context_name: workset[:context_name],
				context_description: workset[:context_description],
				scenarios: workset[:scenarios],
				filename: filename
			})
		end

	end

	describe 'Get Screenshot Files' do
	end

end