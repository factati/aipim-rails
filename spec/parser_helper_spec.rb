#require 'spec_helper'
require './lib/aipim-rails'

describe Parser do

	let(:file) { double(File) }
	let(:line) { "this is the input string" }

	describe "Is command?" do
		before do
			ParserHelper.stub(:is_feature?).with(line).and_return(false)
			ParserHelper.stub(:is_scenario?).with(line).and_return(false)
			ParserHelper.stub(:is_context?).with(line).and_return(false)
			ParserHelper.stub(:is_tag?).with(line).and_return(false)
		end

		describe 'yes' do

			it 'feature' do
				ParserHelper.stub(:is_feature?).with(line).and_return(true)
				expect(ParserHelper.is_command?(line)).to eq(true)
			end

			it 'scenario' do
				ParserHelper.stub(:is_feature?).with(line).and_return(true)
				expect(ParserHelper.is_command?(line)).to eq(true)
			end

			it 'context' do
				ParserHelper.stub(:is_feature?).with(line).and_return(true)
				expect(ParserHelper.is_command?(line)).to eq(true)
			end

			it 'tag' do
				ParserHelper.stub(:is_feature?).with(line).and_return(true)
				expect(ParserHelper.is_command?(line)).to eq(true)
			end

		end

		it 'no' do
			expect(ParserHelper.is_command?(line)).to eq(false)
		end
	end

	describe 'Is feature?' do

		it 'yes' do
			expect(ParserHelper.is_feature?("Funcionalidade: Essa é minha funcionalidade")).to be_true
		end

		it 'no' do
			expect(ParserHelper.is_feature?("not feature")).to be_false
		end

	end

	describe 'Get feature' do

		it 'with name' do
			name = ParserHelper.get_feature("Funcionalidade: this is the name")
			expect(name).to eq("this is the name")
		end

		it 'without name' do
			name = ParserHelper.get_feature("Funcionalidade:")
			expect(name).to eq("")
		end

	end

	describe 'Is scenario?' do

		it 'yes' do
			expect(ParserHelper.is_scenario?("Cenário: this is my scenario")).to be_true
		end

		it 'no' do
			expect(ParserHelper.is_scenario?("not scenario")).to be_false
		end

	end

	describe 'Get scenario' do

		it 'with name' do
			name = ParserHelper.get_scenario("Cenário: this is my scenario")
			expect(name).to eq("this is my scenario")
		end

		it 'without name' do
			name = ParserHelper.get_scenario("Cenário:")
			expect(name).to eq("")
		end
	end

	describe 'Is context?' do

		it 'yes' do
			expect(ParserHelper.is_context?("Contexto: this is my context")).to be_true
		end

		it 'no' do
			expect(ParserHelper.is_context?("not context")).to be_false
		end

	end

	describe 'Get context' do

		it 'with name' do
			name = ParserHelper.get_context("Contexto: this is my context")
			expect(name).to eq("this is my context")
		end

		it 'without name' do
			name = ParserHelper.get_context("Contexto:")
			expect(name).to eq("")
		end
	end

	describe 'Is tag?' do

		it 'yes' do
			expect(ParserHelper.is_tag?("@sometag @someothertag")).to be_true
		end

		it 'no' do
			expect(ParserHelper.is_tag?("not tag")).to be_false
		end

	end

	describe 'Is screenshot?' do

		it 'yes' do
			expect(ParserHelper.is_screenshot?("@javascript @screenshot")).to be_true
		end

		it 'no' do
			expect(ParserHelper.is_screenshot?("@javascript @notscreenshot")).to be_false
		end

	end

	describe 'Is comment?' do

		it 'yes' do
			expect(ParserHelper.is_comment?("# tihs is a comment")).to be_true
		end

		it 'no' do
			expect(ParserHelper.is_comment?("not comment")).to be_false
		end

	end

	describe 'Read line' do

		it 'normal' do
			file.stub(:gets).and_return("This is a string")
			expect(ParserHelper.read_line(file)).to eq("This is a string")
		end

		it 'with extra spaces' do
			file.stub(:gets).and_return("    This    is    another string   ")
			expect(ParserHelper.read_line(file)).to eq("This is another string")
		end

		it 'blank' do
			file.stub(:gets).and_return("   ")
			expect(ParserHelper.read_line(file)).to eq("")
		end

		it 'nil' do
			file.stub(:gets).and_return(nil)
			expect(ParserHelper.read_line(file)).to eq(nil)
		end

	end

end
