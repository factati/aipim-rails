#require 'spec_helper'
require './lib/aipim-rails'

describe Parser do

=begin
	let(:feature) {{
		filename: "myfeature",
		feature_name: "featurename",
		scenarios: [
			{name: "scenario1"},
			{name: "scenario2"}
		]
	}}

	let(:features) { [feature] }

	let(:file) { double(File) }

	describe "Init" do
		before do
			Markdown.stub(:write_feature)
			Markdown.stub(:read_content)
			File.stub(:new).and_return(file)
			file.stub(:close)
		end

		it 'should create markdown feature file' do
			File.should_receive(:new).with("aipim/markdown/myfeature", "w").once
			Markdown.init(features)
		end

		it 'should call write_feature()' do
			Markdown.should_receive(:write_feature).with(feature, file)
			Markdown.init(features)
		end
	end

	describe 'Write Feature' do
		before do
			file.stub(:puts)
		end
		it 'correct output' do
			file.should_receive(:puts).with("[1. scenario1](#)  ")
			file.should_receive(:puts).with("[2. scenario2](#)  ")
			file.should_receive(:puts).with("")
			file.should_receive(:puts).with("# featurename #")
			Markdown.write_feature(feature, file)
		end
	end
=end
end