Gem::Specification.new do |s|
  s.name        = 'aipim-rails'
  s.version     = '0.0.182'
  s.date        = '2014-12-12'
  s.summary     = "Teste aipim"
  s.description = "Teste aipim"
  s.authors     = ["Facta TI"]
  s.email       = 'jefferson@factati.com'
  s.files       = ["lib/aipim-rails.rb",
                  "lib/aipim-rails/parser.rb",
                  "lib/aipim-rails/parser_helper.rb",
                  "lib/aipim-rails/html.rb",
                  "lib/webdriver/screenshot.rb",
                  "lib/assets/bootstrap.min.css",
                  "lib/assets/jquery-1.9.1.js",
                  "lib/assets/costum.css",
                  "lib/assets/costum.js",
                  "lib/assets/header.html",
                  "config/aipim.yml"]
  s.homepage    = 'http://rubygems.org/gems/aipim'
  s.license     = 'FactaTI'
  s.executables = 'aipim'
  s.add_dependency 'cucumber-rails'
  s.add_dependency 'selenium-webdriver'
end
